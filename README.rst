=======
img2org
=======


.. image:: https://img.shields.io/pypi/v/img2org.svg
        :target: https://pypi.python.org/pypi/img2org

.. image:: https://img.shields.io/travis/supersubscript/img2org.svg
        :target: https://travis-ci.org/supersubscript/img2org

.. image:: https://readthedocs.org/projects/img2org/badge/?version=latest
        :target: https://img2org.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Image to Organism simulations


* Free software: GNU General Public License v3
* Documentation: https://img2org.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
