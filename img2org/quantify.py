#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 11:34:13 2020

@author: henrik
"""

import numpy as np
from skimage.morphology import binary_dilation
from skimage.measure import marching_cubes
from imgmisc import autocrop, cut
import mahotas as mh
import numpy as np
from scipy import ndimage as ndi
import networkx as nx

# def get_wall(seg_img, cell1, cell2, selem=None, intensity_image=None, crop=True):
#     """
#     Finds voxels of cell walls between 2 specified cells.
#     Output is a 3D numpy array for each cell i containing:
#         True on voxels of cell i neighbouring the other cell
#         False otherwise

#     """
#     _, cuts = autocrop((seg_img == cell1) | (seg_img == cell2), n=1, threshold=0, return_cuts=True, offset=1)
#     seg_section = cut(seg_img, cuts)
#     border = mh.border(seg_section, cell1, cell2, Bc=selem)
    
#     if crop:
#         border, cuts = autocrop(border, n=1, threshold=0, return_cuts=True, offset=1)
#         seg_section = cut(seg_section, cuts)
        
#     wall1 = border & (seg_section == cell1)
#     wall2 = border & (seg_section == cell2)

#     if intensity_image is not None:
#         if crop:
#             intensity_image = cut(intensity_image, cuts)
#         vals1 = sum(intensity_image[wall1])
#         vals2 = sum(intensity_image[wall2])
#         return wall1, wall2, vals1, vals2
    
#     return wall1, wall2


def get_wall(seg_img, cell1, cell2, selem=None, intensity_image=None, fct=sum):
    """
    Finds voxels of cell walls between 2 specified cells.
    Output is a 3D numpy array for each cell i containing:
        True on voxels of cell i neighbouring the other cell
        False otherwise
    """
    _, cuts = autocrop(np.isin(seg_img, [cell1, cell2]), n=1, threshold=0, return_cuts=True, offset=1)
    seg_section = cut(seg_img, cuts)
    border = mh.border(seg_section, cell1, cell2, Bc=selem)
        
    wall1 = np.logical_and(border, seg_section == cell1)
    wall2 = np.logical_and(border, seg_section == cell2)

    if intensity_image is not None:
        intensity_image = cut(intensity_image, cuts)
        vals1 = fct(intensity_image[wall1])
        vals2 = fct(intensity_image[wall2])
        return wall1, wall2, vals1, vals2
    return wall1, wall2

def get_l1(seg_img, bg=0, selem=None):
    # TODO: Include option to remove cells bordering the image limits?
    bg_dilated = np.logical_and(binary_dilation(seg_img==bg, selem=selem), seg_img)
    
    labels = np.unique(seg_img)
    labels = labels[labels != bg]
    l1 = np.unique(seg_img[bg_dilated])
    l1 = l1[l1 != bg]
    l1 = np.isin(labels, l1)
    return l1

def get_layers(seg_img, bg=0, depth=None):
    labeled_img = seg_img.copy()
    layers = []

    while True:
        if depth is not None and len(layers) >= depth:
            break
        
        layer = get_l1(labeled_img, bg=bg)
        
        if sum(layer) == 0:
            continue
        layer_labels = np.unique(labeled_img)[1:][layer]
        layers.append(layer_labels)
        labeled_img[np.isin(labeled_img, layer_labels)] = bg
    return layers

def area_between_walls(wall1, wall2, resolution=(1, 1, 1)):
    """
    Finds surface area of contact between 2 specified cell walls.
    Output is a float value, with units [voxel_dim units]**2
    
    wall1 --- 3D numpy array containing True on voxels of wall1, as ouputted
              by the get_wall function
    wall2 --- 3D numpy array of the same size for wall2
    voxel_dims --- 3-tuple of voxel dimensions
    """
    # TODO not sure why order matters?
    resolution = tuple(resolution)  # in case list object was given
    
    # Create mesh of walls using marching cubes algorithm
    verts1, faces1, _, _ = marching_cubes(wall1, level=0.5, spacing=resolution, allow_degenerate=False)
    verts2, faces2, _, _ = marching_cubes(wall2, level=0.5, spacing=resolution, allow_degenerate=False)
    verts1, verts2 = list(map(tuple, verts1)), list(map(tuple, verts2))
    actual_area = 0
    for _ in range(2):
        verts1, faces1 = verts2, faces2
        # Find vertices in mesh common between walls
        my_verts = []
        vert_inds = []
        for i, v in enumerate(verts1):
            if v in verts2:
                my_verts.append(v)
                vert_inds.append(i)
        my_verts = np.array(my_verts)
        
        # Keep triangles which connect these vertices
        my_faces = []
        for f in faces1:
            if set(f).issubset(vert_inds):
                my_faces.append(f)
        my_faces = np.array(my_faces, dtype=int)
        
        # # If no faces in common, return 0 surface area
        if my_faces.size == 0:
            return 0
        
        # Relabel vertex indices in my_faces
        relab = np.unique(vert_inds)
        for ii, val in enumerate(relab):
            my_faces[my_faces == val] = ii
    
        # Calculate surface area of new mesh
        # Fancy indexing to define two vector arrays from triangle vertices
        actual_verts = my_verts[my_faces]
        a = actual_verts[:, 0, :] - actual_verts[:, 1, :]
        b = actual_verts[:, 0, :] - actual_verts[:, 2, :]
        del actual_verts
    
        # Area of triangle in 3D = 1/2 * Euclidean norm of cross product
        area = ((np.cross(a, b) ** 2).sum(axis=1) ** 0.5).sum() / 2.
        actual_area += area
    actual_area /= 2
    return actual_area

def quantify_signal(image, voxels):
    """
    Measures total signal (e.g. PIN) on specified 'True' voxels.
    
    image --- 3D numpy array of image signal (e.g. intensity of PIN:GFP)
    voxels --- 3D numpy array of same size, containing bools.
    """
    values = np.array(image[voxels == True])
    total_signal = np.sum(values)
    return total_signal

def set_background(seg_img, background=0, mode='largest'):
    labels, counts = np.unique(seg_img, return_counts=True)
    swap_label = labels[np.argmax(counts)]
    
    old_background = seg_img == background
    new_background = seg_img == swap_label
    seg_img[old_background] = swap_label
    seg_img[new_background] = background
    
    return seg_img

def _validate_inputs(image, mask, resolution):
    """Ensure that all inputs to segmentation have matching shapes and types.
    Modified from the Scikit-Image watershed code.

    Parameters
    ----------
    image : array
        The input image.
    mask : array, or None
        A boolean mask, True where we want to segment.
    resolution : array, or None
        Resolution per dimension of the input image.

    Returns
    -------
    image, mask, resolution: arrays
        The validated and formatted arrays. Image and resolution will have dtype
        float64, and mask int8. If ``None`` was given for the mask, it is a volume
        of all 1s.

    Raises
    ------
    ValueError
        If the shapes of the given arrays don't match.
    """

    if mask is not None and mask.shape != image.shape:
        raise ValueError("`mask` must have same shape as `image`")
    if mask is None:
        # Use a complete `True` mask if none is provided
        mask = np.ones(image.shape, bool)

    if not isinstance(image, np.ndarray):
        image = np.array(image)
    if not isinstance(mask, np.ndarray):
        mask = np.array(mask)
    if resolution is None:
        resolution = np.ones((image.ndim), np.float64)
    elif not isinstance(resolution, np.ndarray):
        resolution = np.array(resolution)
    if len(resolution) != image.ndim:
        raise ValueError(
            "`resolution` must have a value per dimension of `image`")

    return (image,
            mask,
            resolution)


def _validate_connectivity(image_dim, connectivity, offset):
    """Convert any valid connectivity to a structuring element and offset.

    Parameters
    ----------
    image_dim : int
        The number of dimensions of the input image.
    connectivity : int, array, or None
        The neighbourhood connectivity. An integer is interpreted as in
        ``scipy.ndimage.generate_binary_structure``, as the maximum number
        of orthogonal steps to reach a neighbor. An array is directly
        interpreted as a structuring element and its shape is validated against
        the input image shape. ``None`` is interpreted as a connectivity of 1.
    offset : tuple of int, or None
        The coordinates of the center of the structuring element.

    Returns
    -------
    c_connectivity : array of bool
        The structuring element corresponding to the input `connectivity`.
    offset : array of int
        The offset corresponding to the center of the structuring element.

    Raises
    ------
    ValueError:
        If the image dimension and the connectivity or offset dimensions don't
        match.
    """
    if connectivity is None:
        connectivity = 1

    if np.isscalar(connectivity):
        c_connectivity = ndi.generate_binary_structure(image_dim, connectivity)
    else:
        c_connectivity = np.array(connectivity, bool)
        if c_connectivity.ndim != image_dim:
            raise ValueError("Connectivity dimension must be same as image")

    if offset is None:
        if any([x % 2 == 0 for x in c_connectivity.shape]):
            raise ValueError("Connectivity array must have an unambiguous "
                             "center")

        offset = np.array(c_connectivity.shape) // 2

    return c_connectivity, offset


def _compute_neighbours(image, structure, offset):
    """Compute neighbourhood as an array of linear offsets into the image.

    These are sorted according to Euclidean distance from the center (given
    by `offset`), ensuring that immediate neighbours are visited first.
    """
    structure[tuple(offset)] = 0  # ignore the center; it's not a neighbor
    locations = np.transpose(np.nonzero(structure))
    sqdistances = np.sum((locations - offset)**2, axis=1)
    neighbourhood = (np.ravel_multi_index(locations.T, image.shape) -
                     np.ravel_multi_index(offset, image.shape))
    sorted_neighbourhood = neighbourhood[np.argsort(sqdistances)]
    return sorted_neighbourhood


def get_footprint(dimensions, connectivity, offset=None):
    """ Get footprint. """
    return _validate_connectivity(dimensions, connectivity, offset)[0]

def find_neighbors(image, background=0, connectivity=1):
    image, mask, resolution = _validate_inputs(image, mask=None, resolution=None)
    connectivity, offset = _validate_connectivity(image.ndim, connectivity,
                                                  offset=None)
    mask = mh.borders(image, connectivity)

    # get the distances to the neighbours
    neighbours = connectivity.copy()
    neighbours[tuple(offset)] = False
    neighbours = np.array(np.where(neighbours)).T
    neighbours = np.multiply(neighbours, resolution)
    neighbours = np.subtract(neighbours, offset)

    # pad the image, and mask so that we can use the mask to keep from running
    # off the edges
    pad_width = [(p, p) for p in offset]
    image = np.pad(image, pad_width, mode='constant')
    mask = np.pad(mask, pad_width, mode='constant')

    # get flattened versions of everything
    flat_neighbourhood = _compute_neighbours(image, connectivity, offset)
    image_raveled = image.ravel()
    indices = np.where(mask.ravel())[0]
    
    point_neighbours = np.array(list(map(lambda x: x + flat_neighbourhood, indices)))
    neighs = {ii:[] for ii in np.unique(image_raveled[indices])}# {image_raveled[indices[ii]] : [] for ii in len(range(indices))}
    for ii, ptn in enumerate(point_neighbours):
        neighs[image_raveled[indices[ii]]].append(image_raveled[ptn])
    neighs = {ii:np.unique(neighs[ii]) for ii in neighs.keys()}
    neighs = {ii:neighs[ii][np.logical_and(neighs[ii] != background, neighs[ii] != ii)]  for ii in neighs.keys()}
    del neighs[background]

    return neighs

# def find_neighbors(seg_img, bg=0, include_self=False, ball_radius=1):
    
    
    
#     # from scipy.spatial import cKDTree
#     # import mahotas as mh
#     # import collections
#     # from imgmisc import flatten

#     # borders = mh.borders(seg_img)
#     # borders = np.array(np.where(borders)).T
#     # tree = cKDTree(borders)
#     # n = tree.query_ball_point(borders, ball_radius)
#     # labels = seg_img[borders[:,0], borders[:,1], borders[:,2]]
#     # d = collections.defaultdict(list)
#     # for ii in range(len(n)):
#     #     coords = borders[np.asarray(n[ii])]
#     #     d[labels[ii]].append(seg_img[coords[:,0], coords[:,1], coords[:,2]])

#     # p = collections.OrderedDict()
#     # for dd in d.keys():
#     #     print(f'Collating neighbors for cell id {dd}')
#     #     p[dd] = np.unique(flatten(d[dd]))
#     #     p[dd] = p[dd][p[dd] != bg]
#     #     if not include_self:
#     #         p[dd] = p[dd][p[dd] != dd]
        
#     # del p[bg]

#     return p  

# def find_neighbors(seg_img, bg=None, selem=None):
#     """
#     Output dictionary with the n-th entry being a list of labels of regions
#     which border the n-th region, and a list of the respective contact areas
    
#     img_in --- 3D array of region labels
# #    ignoreZero --- Boolean to say whether regions with label 0 are ignored
#     voxel_dims --- 3-tuple of voxel dimensions, used to calculate areas
#     """
#     from imgmisc import autocrop, cut
#     labels = np.unique(seg_img)
#     if bg is not None:
#         labels = labels[labels != bg]
    
#     neighs = {}
#     for region in labels:
        
#         print("Finding neighbors of " + str(region))
#         mask, cuts = autocrop(seg_img == region, n=1, threshold=0, offset=2, return_cuts=True)
#         seg_section = cut(seg_img, cuts)
#         mask = binary_dilation(mask, selem=selem)
#         cell_neighbours = np.unique(seg_section[mask])
#         cell_neighbours = cell_neighbours[cell_neighbours != region]
    
#         if bg is not None:
#             cell_neighbours = cell_neighbours[cell_neighbours != bg]
        
#         neighs[region] = cell_neighbours
        
#     return neighs
