# -*- coding: utf-8 -*-

"""Top-level package for img2org."""
import warnings

__author__ = """Henrik Åhl"""
__email__ = 'hpa22@cam.ac.uk'
__version__ = '0.4.0'
