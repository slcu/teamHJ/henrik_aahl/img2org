#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 11:36:05 2020

@author: henrik
"""
import re
import pandas as pd
import numpy as np

def read_output_gnuplot(fname, skiprows=0):
    return pd.read_csv(fname, sep=' ', header=None, squeeze=True, skiprows=skiprows).dropna(1).dropna(0)

def read_init(fname):
    output = pd.read_csv(fname, sep=' ', header=None, squeeze=True, skiprows=1).dropna(1).dropna(0)
    return output

def read_neigh_init(fname):
    # TODO: rewrite to have same format as read_neigh
    with open(fname, 'r') as f:
        firstline = f.readline().split(' ')
        n_cells, n_params = int(firstline[0]), int(firstline[1])
        data = {'n_cells':n_cells, 'n_params':n_params}
        for line in f:
            vals = line.split(' ')
            data[int(vals[0])] = {}
            data[int(vals[0])]['n_neighs'] = int(vals[1])
            offset = 2 + data[int(vals[0])]['n_neighs']
            data[int(vals[0])]['neighs'] = np.array(vals[2:offset], 'int')
            for ii in range(n_params):
                data[int(vals[0])][f'p{ii}'] = np.array(vals[offset:offset + data[int(vals[0])]['n_neighs']], float)
                offset += data[int(vals[0])]['n_neighs']
    return data
    
def read_neigh(fname):
    # with open(fname, 'r') as f:
        f = open(fname, 'r') 
        data = {}
        t_data = {}
        piter = 0
        for line in f:
            if len(line.strip()) == 0:
                data[piter] = t_data
                t_data = {}
                piter += 1
            else:
                vals = line.split(' ')
                c_data = {'iter':int(vals[0]),
                          't': float(vals[1]), 
                          'cell_id' : int(vals[2]),
                          'n_neighs': int(vals[3])}
                c_data['neighs'] = vals[4:4 + c_data['n_neighs']]
                for ii in range(4 + c_data['n_neighs'], len(vals), c_data['n_neighs']):
                    c_data[f'p{int((ii - 4 - c_data["n_neighs"]) / c_data["n_neighs"])}'] = np.array(vals[ii:ii + c_data['n_neighs']], float)
                t_data[c_data['cell_id']] = c_data
        return data
            
def read_output_newman(fname):
    with open(fname, 'r') as f:
        # fname = '/home/henrikahl/projects/pin_patterns/simulations/1Dgrowth/organism/test_growth.dat'
        # f = open(fname, 'r') 
        data = {}
        t_data = []
        piter = 0
        line = f.readline()
        line = f.readline()
        for line in f:
            # line = f.readline()
            if len(line.strip()) == 0:
                line = f.readline()
                data[piter] = np.array(t_data)
                t_data = []
                piter += 1
            else:
                vals = np.array(line.split(' '), dtype=float)
                t_data.append(vals)
        return data


def write_init(fname, init):
    s = f'{init.shape[0]} {init.shape[1]}\n'
    s += init.to_string(header=False, index=False)
    s = s.split('\n')
    s = [re.sub('\s+', ' ', line).lstrip() for line in s]
    s = '\n'.join(s)
    with open(fname, 'w') as f:
        f.write(s)

def write_neigh_init(fname, neigh_init):
    with open(fname, 'w') as f:
        n_cells = neigh_init["n_cells"]
        n_params = neigh_init["n_params"]
        f.write(f'{n_cells} {n_params}\n')
        
        del neigh_init['n_cells'], neigh_init['n_params']
        for key, value in neigh_init.items():
            f.write(f'{key} {value["n_neighs"]} {" ".join(map(str, value["neighs"]))}')
            for ii in range(n_params):
                sub_key = f'p{ii}'
                f.write(f' {" ".join(map(str, value[sub_key]))}')
            f.write('\n')
            

def create_organism_files(my_dict, init_filepath, neigh_filepath):
    """
    Converts dicionary of cell/PIN data into init and neigh text files which
    can be used by the Organism solver.
    
    Saves data in specified filepaths.
    """
    init = str(len(my_dict)) + " 8"
    neigh = str(len(my_dict)) + " 4"
    
    mean_PIN = np.mean([my_dict[r]['total_PIN'] for r in range(len(my_dict))])
    # normalised_PIN = np.array([my_dict[r]['total_PIN'] / mean_PIN for r in range(len(my_dict))])
    
    vals = []
    for r in range(len(my_dict)):
        num_neighs = len(my_dict[r]["neighbors"])
        vals.append([])
        for jj in range(num_neighs):
            val = my_dict[r]["P_ij"][jj]
            vals[-1].append(val)
    
    for r in range(len(my_dict)):
        init += "\n" + " ".join(str(n) for n in my_dict[r]["coords"])
        init += " " + str(float(my_dict[r]["volume"]))
        
        # PIN, auxin, AUX concentrations
        total_PIN = float(my_dict[r]["total_PIN"])
        for ii in range(3):
            init += " " + str(total_PIN)
        init += " " + str(int(my_dict[r]["L1"])) + " "
        
        num_neighs = len(my_dict[r]["neighbors"])
        neigh += "\n" + str(r) + " " + str(num_neighs) # zero indexed
        neigh += " " + " ".join(str(n-1) for n in my_dict[r]["neighbors"])
        
        ### Area
        # 
        neigh += " " + " ".join(str(n) for n in my_dict[r]["A_ij"])

        ### PIN1
        # PIN1 is normalised to be on average 1 in each cell. The total membrane 
        # bound PIN must therefore be averaged with the average total PIN in the 
        # tissue. PIN is printed twice for historical reasons.
        for ii in range(2):
            for jj in range(num_neighs):
                neigh += " " + str(my_dict[r]["P_ij"][jj] / mean_PIN)
                
        ### AUX1
        # AUX1 is proportional to PIN1, but symmetrically distributed on the cell 
        # membranes. We therefore take AUX1 to be the total (normalised) PIN in
        # the cell, and distribute it equally over the cell membranes.
        for ii in range(num_neighs):
            value = total_PIN / mean_PIN / num_neighs if num_neighs > 0 else 0
            neigh += " " + str(value)

    with open(init_filepath, 'w') as f1:
        f1.write(init)
    with open(neigh_filepath, 'w') as f2:
        f2.write(neigh)


def gnuplot2newman(fin, fout, n_params, use_every=1, verbose=False):
    data = read_output_gnuplot(fin)
    
    # Account for the number of columns, as this varies between newman and gnuplot
    data = data.loc[:, 0:n_params + 3] # include time, printpoint and cell_id
    data.columns = ['print', 't', 'cell_id', 'n_neighs', 'x', 'y', 'z', 'volume'] + [f'var_{ii}' for ii in range(data.columns.size - 8)]
    data = data.drop_duplicates(['print', 't', 'cell_id'])

    # Get some general parameters
    n_cells = data['cell_id'].unique().shape[0] #len(np.unique(data['cell_id']))
    n_print = data['print'].unique().shape[0]  #len(np.unique(data['print']))
    times = data.drop_duplicates('print').t.values
    
    # Rearrange columns to newman order
    columns = [v for v in data.columns if v not in ['t', 'cell_id', 'n_neighs']] + ['n_neighs']
    data = data.loc[:, columns]

    # Write to file    
    with open(fout, 'w') as f:
        f.write(f'{n_print//use_every}\n')
    
        for ii in range(0, n_print, use_every):
            # Get right time-point and remove pesky whitespaces
            outstring = data.loc[data['print'] == ii, columns[1:]].to_string(index=False, header=False)
            outstring = re.sub('\n\s+', '\n', outstring).strip()
            
            if verbose:
                print(f'Printing time-point {ii//use_every} out of {n_print // use_every}')
            f.write(f'{n_cells} {n_params + 1} {times[ii]}\n')
            f.write(outstring)
            f.write('\n\n')
    